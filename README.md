# sway-randr - swaymsg wrapper for xrandr

This python3 script allow you to convert xrandr scripts to swaymsg. It only
support a subset of xrandr options.

A way to convert script, and make then both compatible with X and Sway, is to
put your sway-randr script somewhere in your $PATH, and then to add at the
begining of your scripts using xrandr:

```if [ $SWAYSOCK ]; then alias xrandr=sway-randr ; fi```
